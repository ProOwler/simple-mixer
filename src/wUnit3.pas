unit wUnit3;

interface    

uses
  StrUtils, SysUtils, Classes;

type
  TSimpleStrList = class
  public
    list: array of string;
    constructor Create;
    function getLength: integer;
    procedure Add(s: string);
  private
    length: integer;
  end;

  StringMaker = class
  private
    staticL: TSimpleStrList;
    resStr: string;
  public
    constructor Create(template: TSimpleStrList);
    procedure LoadSourceStr(source, delimiter: string);
    function GetResStr: string;
    function Shred(orig, mask: string): TSimpleStrList;
  end;

implementation

{ StringMaker }

constructor StringMaker.Create(template: TSimpleStrList);
begin
 inherited Create;
 staticL:= TSimpleStrList.Create; // (template.length)
 staticL:= template;
end;

function StringMaker.Shred(orig, mask: string): TSimpleStrList;
var
 i: integer;
 tempStr: string;
 rez: TSimpleStrList;
begin
 rez:= TSimpleStrList.Create;
 while(pos(mask, orig) > 0) do begin
  tempStr:= LeftStr(orig, pos(mask, orig)-1);
  Delete(orig, 1, pos(mask, orig) + length(mask)-1);
  rez.Add(tempStr);
 end;
 rez.Add(orig);
 Shred:= rez;
end;

function StringMaker.GetResStr: string;
begin
 GetResStr:= resStr;
end;

procedure StringMaker.LoadSourceStr(source, delimiter: string);
var
 rez: string;
 dynL: TSimpleStrList;
 sCount, dCount, i: integer;
begin
 sCount:= staticL.GetLength;
 if sCount > 0 then begin
  if sCount = 1 then rez:= staticL.list[0] + source
  else begin
   dynL:= TSimpleStrList.Create;
   dynL:= Shred(source, delimiter);
   dCount:= dynL.GetLength;
   i:= 0;
   repeat
    rez:= rez + staticL.list[i] + dynL.list[i];
    i:= i+1;
    sCount:= sCount - 1;
    dCount:= dCount - 1;
   until (sCount = 1)or(dCount = 0);
   if((sCount = 1)and(dCount = 0)) then rez:= rez + staticL.list[i];
   if(sCount > 1) then begin
    repeat
     rez:= rez + staticL.list[i];
     i:= i+1;
     sCount:= sCount - 1;
    until(sCount = 0);
   end;
   if(dCount > 0) then begin
    repeat
     rez:= rez + dynL.list[i];
     i:= i+1;
     dCount:= dCount - 1;
    until(dCount = 0);
    rez:= rez + staticL.list[staticL.GetLength-1];
   end;
  end;
 end
 else begin
  rez:= source;
 end;
 resStr:= rez;
end;

{ TSimpleStrList }

constructor TSimpleStrList.Create;
begin
 inherited Create;
 length:= 0;
 SetLength(list, length);
end;

function TSimpleStrList.getLength: integer;
begin
 getLength:= length;
end;

procedure TSimpleStrList.Add(s: string);
begin
 SetLength(list, length+1);
 list[length]:= s;
 inc(length);
end;

end.