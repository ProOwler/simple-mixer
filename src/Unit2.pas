unit Unit2;

interface

uses
  StrUtils, Classes, wUnit3;

type
  listMaker = class
  private
    ResArray: TSimpleStrList;
    length: integer;
  public
    constructor Create(L: integer);
    procedure putStrArray(mask: string; template, source: TStrings);
    function getResList: TSimpleStrList;
  end;

implementation

constructor listMaker.Create(L: integer);
begin
 inherited Create;
 length:= L;
 ResArray:= TSimpleStrList.Create;
end;

procedure listMaker.putStrArray(mask: string; template, source: TStrings);
var
 baseStrParts: TSimpleStrList;
 worker: StringMaker;
 temp: string;
 i: integer;
begin
 baseStrParts:= TSimpleStrList.Create;
 for i:= 0 to template.Count-1 do //usually 5 parts
  baseStrParts.Add(template.Strings[i]);
 worker:= StringMaker.Create(baseStrParts);
 temp:='';
 for i:= 0 to source.Count-1 do begin
  worker.LoadSourceStr(source.Strings[i], mask);
  ResArray.Add(worker.GetResStr);
 end;
 worker.Free;
end;

function listMaker.getResList: TSimpleStrList;
begin
 getResList:= ResArray;
end;

end.

